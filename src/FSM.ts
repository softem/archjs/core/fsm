import {ITransition}        from "./ITransition";
import {IFSM}               from "./IFSM";
import {IState}             from "./IState";
import {ITransitionStream}  from "./ITransitionStream";

interface FSMData{
    states:Object;
    transitions:Array<ITransition>;
    initial?:string;
}

class FSM implements IFSM {
    protected _states:     { [key: string]: IState; } = {};
    protected _transitions:Array<ITransition>;
    protected _current:    IState;
    protected _previous:   IState;

    get stateName(){
        return this._current.name;
    }
    get state(){
        return this._current.state;
    }

    protected createStatesAndTransitions( data:FSMData ){
        for(var key in data.states ){
            this._states[key] = <IState>{};
            this._states[key].transitions = [];
            this._states[key].state = data.states[key];
            this._states[key].name  = key;
            for( var i=0, length = data.transitions.length; i<length; i++ ){
                if( data.transitions[i].from == key ){
                    this._states[key].transitions.push(data.transitions[i]);
                }
            }
        }
        this._transitions = data.transitions;
    }

    public constructor( data ){
        this.createStatesAndTransitions( data );
        var initStateKey = (data.initial)?data.initial : Object.keys(this._states)[0];
        this._current = this._states[initStateKey];
    }

    /**
     * @inheritDoc
     */
    public next(input?) {
        return this.transition( input );
    }

    /**
     * @inheritDoc
     */
    public transition(input?:any, newState?:string) {
        var transitionStream:ITransitionStream = {
            input:     input,
            current:   this._current.name,
            previous:  (this._previous)?this._previous.name:this._current.name,
            eventName: ''
        };
        for( var i=0; i<this._current.transitions.length; i++) {
            if( newState && this._current.transitions[i].to !== newState ) {
                continue;
            }
            if( !this._current.transitions[i].trigger || this._current.transitions[i].trigger( transitionStream ) ) {
                this._previous      = this._current;
                this._current       = this._states[this._current.transitions[i].to];
                transitionStream.current  = this._current.name;
                transitionStream.previous = this._previous.name;
                transitionStream.eventName= this._previous.transitions[i].name;
                return transitionStream;
            }
        }
        var error = '';
        if(newState){
            error = 'Transition from state ' + this._current.name + ' to ' + newState + ' is not defined '
        }else{
            error = 'There are no to states to transition from state ' + this._current.name;
        }
        throw new Error(error);
    }
}

export { FSMData, FSM };
