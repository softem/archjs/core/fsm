interface ITransitionStream {
    input: any;
    current: string;
    previous: string;
    eventName?: string;
}
export { ITransitionStream };
