interface ITransition {
    name: string;
    from: string;
    to: string;
    trigger?: Function;
}
export { ITransition };
