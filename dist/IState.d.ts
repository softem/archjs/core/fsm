import { ITransition } from "./ITransition";
interface IState {
    name: string;
    state: any;
    transitions: Array<ITransition>;
}
export { IState };
