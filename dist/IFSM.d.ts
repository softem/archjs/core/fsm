interface IFSM {
    /**
     * Move to the first possible state
     * @param input
     */
    next(input: any): any;
    /**
     * Moves from current state to specified state
     * only if edge is defined and input data is satisfying the condition attached on the edge
     * @param input
     * @param newState
     */
    transition(input: any, newState: string): any;
}
export { IFSM };
