import { ITransition } from "./ITransition";
import { IFSM } from "./IFSM";
import { IState } from "./IState";
import { ITransitionStream } from "./ITransitionStream";
interface FSMData {
    states: Object;
    transitions: Array<ITransition>;
    initial?: string;
}
declare class FSM implements IFSM {
    protected _states: {
        [key: string]: IState;
    };
    protected _transitions: Array<ITransition>;
    protected _current: IState;
    protected _previous: IState;
    readonly stateName: string;
    readonly state: any;
    protected createStatesAndTransitions(data: FSMData): void;
    constructor(data: any);
    /**
     * @inheritDoc
     */
    next(input?: any): ITransitionStream;
    /**
     * @inheritDoc
     */
    transition(input?: any, newState?: string): ITransitionStream;
}
export { FSMData, FSM };
